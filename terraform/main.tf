provider "azurerm" {
  version         = ">= 2.0"
  features {}
}

data "azurerm_resource_group" "demo-rg" {
  name = var.resource_group
}

output "resource_group_id" {
  value = data.azurerm_resource_group.demo-rg.id
}


resource "azurerm_storage_account" "demo-sa" {
  name                     = var.storage_acc_name
  resource_group_name      = data.azurerm_resource_group.demo-rg.name
  location                 = data.azurerm_resource_group.demo-rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  tags                     = var.tags
}

resource "azurerm_storage_container" "demo-sc" {
  name                  = "content"
  storage_account_name  = azurerm_storage_account.demo-sa.name
  container_access_type = "private"
}
