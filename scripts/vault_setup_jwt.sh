#!/bin/sh

# **Important** VAULT_ADDR, VAULT_TOKEN environment variables should be set prior to running this script
# This script configures the Azure secret Engine in Vault
# Complete up to 2_assign_permissions in Makefile
echo "Please ensure VAULT_ADDR, VAULT_TOKEN, TFC_TOKEN, TFC_ORG and TFC_WORKSPACE is setup environment variables are set."
echo "Optionally please set this variable to setup the JWT Auth method: GITLAB_PROJECT_ID"
scripts/check_vault_vars.sh
if [ ! -f "vars_are_valid" ]; then
    echo "ERR: Failed to verify required Variables."
    exit 1
fi

echo "INFO: Checking connectivity to vault server: ${VAULT_ADDR}, and vault token"
vault status
vault token lookup

echo "INFO: Creating the AppRole Auth Method"
vault auth disable approle
vault auth enable approle
vault write auth/approle/role/gitlab \
    secret_id_ttl=24h \
    token_num_uses=50 \
    token_ttl=20m \
    token_max_ttl=30m \
    secret_id_num_uses=50 \
    policies="gitlab-runner,terraform"

vault read auth/approle/role/gitlab

# Use .data.role_id in role.json file as the ROLE_ID for Gitlab setup
echo "INFO: Role ID written to role.json file"
vault read -format=json auth/approle/role/gitlab/role-id > role.json
export ROLE_ID="$(cat role.json | jq -r .data.role_id )" && echo $ROLE_ID > roleid

# Use .data.secret_id in secretid.json file as the SECRET_ID for Gitlab credential
echo "INFO: Secret ID written to secret.json file"
vault write -format=json -f auth/approle/role/gitlab/secret-id > secretid.json
export SECRET_ID="$(cat secretid.json | jq -r .data.secret_id )" && echo $SECRET_ID > secretid

if [ ! -z "$GITLAB_PROJECT_ID" ]; then
  echo "INFO: Creating the JWT auth method"
  # Create the key/value secrets engine to store Terraform Cloud credentials
  vault auth disable jwt
  vault auth enable jwt

  vault write auth/jwt/config \
    jwks_url="https://gitlab.com/-/jwks" \
    bound_issuer="gitlab.com"

  echo "INFO: Creating a Role for GitLab under the jwt auth method"
  cat <<EOF > gitlab-runner-role.json
  {
    "role_type": "jwt",
    "policies": ["gitlab-runner","terraform"],
    "token_explicit_max_ttl": 300,
    "user_claim": "user_email",
    "bound_claims": {
    "project_id": "$GITLAB_PROJECT_ID",
    "ref": "demo",
    "ref_type": "branch"
    }
  }
EOF

vault write auth/jwt/role/gitlab @gitlab-runner-role.json

echo "NOTE: bound_claims is set to Gitlab project ID: $GITLAB_PROJECT_ID."

echo "INFO: Displaying the jwt auth method and role"
vault read auth/jwt/config
vault read auth/jwt/role/gitlab
  
else
  echo "INFO: Skipping the JWT auth method, please set a VAULT_TOKEN environment variable instead."
fi

echo "INFO: Create the kv2 secrets engine"
# Create the key/value secrets engine to store Terraform Cloud credentials
vault secrets disable kv2
vault secrets enable -path=kv2 -version=2 kv
vault kv put kv2/tfc_token TFC_TOKEN=$TFC_TOKEN

echo "INFO: Creating the Azure secrets engine"
export ARM_CLIENT_ID="$(cat ${CREDS_FILE_PATH} | jq -r .appId)"
export ARM_TENANT_ID="$(cat ${CREDS_FILE_PATH} | jq -r .tenant)"
export ARM_CLIENT_SECRET="$(cat ${CREDS_FILE_PATH} | jq -r .password)"

scripts/validate_vars.sh
vault lease revoke -prefix "${AZ_SECRET_PATH}/"
#sleep 10
#vault lease revoke -force -prefix "${AZ_SECRET_PATH}/"

# Create secrets engine
vault secrets disable ${AZ_SECRET_PATH}
vault secrets enable -path=${AZ_SECRET_PATH} azure
vault write ${AZ_SECRET_PATH}/config \
  subscription_id=${ARM_SUBSCRIPTION_ID} \
  tenant_id=${ARM_TENANT_ID} \
  client_id=${ARM_CLIENT_ID} \
  client_secret=${ARM_CLIENT_SECRET}
vault secrets tune -default-lease-ttl=600s -max-lease-ttl=900s ${AZ_SECRET_PATH}

# Create Role
scripts/create_role.sh

# Check if az CLI is installed
t="az"
if [[ -x "$(command -v $t)" ]]; then
    # Create Resource Group if exists
    echo "Checking if Resource group exists"
    group_exists=$(az group exists --name ${RESOURCE_GROUP})
    echo "... $group_exists"
    if [[ $group_exists == "false" ]]; then
        echo "Creating resource group: $RESOURCE_GROUP"
        az group create -l westus -n $RESOURCE_GROUP 
    fi
else
      echo "WARN: Could not check for Resource Group existense since $t is not installed. Assuming the Resource_Group $RESOURCE_GROUP is already created."
fi

echo "INFO: Creating policy for Terraform"
# Please ensure this is consistent with create_tf_policy.sh
vault policy write terraform -<<EOH
path "${AZ_SECRET_PATH}/creds/${RESOURCE_GROUP}-role" {
  capabilities = ["read"]
}
EOH

echo "INFO: Creating policy for GitLab Runner"
# Create a policy for GitLab runners
vault policy write gitlab-runner -<<EOH
path "kv2/data/tfc_token" {
  capabilities = ["read"]
}
EOH

echo "INFO: Creating token for GitLab Runner"
# Create a token for the Runners with a TTL of 24 hours
vault token create -policy=gitlab-runner -policy=terraform -ttl=24h

token=$(vault token create -format=json -policy=gitlab-runner -policy=terraform -ttl=24h | jq -r .auth.client_token)
echo "INFO: Running test with token: $token"
VAULT_TOKEN=$token vault read -format=json ${AZ_SECRET_PATH}/creds/${RESOURCE_GROUP}-role \
  && vault read -format=json kv2/data/tfc_token | grep TFC_TOKEN

echo "INFO: Testing AppRole Auth for Gitlab Runner"
token=$(vault write -format=json auth/approle/login role_id=$ROLE_ID secret_id=$SECRET_ID | jq -r .auth.client_token)
echo "INFO: Running test with token: $token"
VAULT_TOKEN=$token vault read -format=json ${AZ_SECRET_PATH}/creds/${RESOURCE_GROUP}-role \
  && vault read -format=json kv2/data/tfc_token | grep TFC_TOKEN

echo "INFO: script completed. Please configure a VAULT_TOKEN variable in GitLab CI/CD Variables with value: $token"
echo "To test: VAULT_TOKEN=$token vault read -format=json ${AZ_SECRET_PATH}/creds/${RESOURCE_GROUP}-role && vault read -format=json kv2/data/tfc_token | grep TFC_TOKEN"
