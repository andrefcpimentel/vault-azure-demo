#!/bin/sh

[[ -z ${ARM_SUBSCRIPTION_ID} ]] && echo "ERROR: Please set the ARM_SUBSCRIPTION_ID variable"

[[ -z ${ARM_CLIENT_ID} ]] && echo "ERROR: Please set the ARM_CLIENT_ID variable"

# Create owner Role assignment
az role assignment create --assignee ${ARM_CLIENT_ID} \
  --role "Owner" \
  --subscription ${ARM_SUBSCRIPTION_ID}

# Note: the following Resource Accessids were obtained by first configuring a user with the UI, then downloading an application manifest: https://medium.com/r/?url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Fazure%2Factive-directory%2Fdevelop%2Freference-app-manifest 
# The following website provides a nice summary table: https://www.cloudidentity.com/blog/2015/09/01/azure-ad-permissions-summary-table/

# Setting permission ID variables
resourceAccessid1="311a71cc-e848-46a1-bdf8-97ff7156d8e6=Scope"
resourceAccessid2="1cda74f2-2616-4834-b122-5cb1b07f8a59=Role"
resourceAccessid3="78c8a3c8-a07e-4b9e-af1b-b5ccab50a175=Role"
resourceAppId=00000002-0000-0000-c000-000000000000

# Assign permissions
az ad app permission add --id ${ARM_CLIENT_ID} \
  --api ${resourceAppId} \
  --api-permissions ${resourceAccessid1} ${resourceAccessid2} ${resourceAccessid3}

# Admin grant
az ad app permission grant --id ${ARM_CLIENT_ID} --api ${resourceAppId}

# Grant Admin consent
az ad app permission admin-consent --id ${ARM_CLIENT_ID}

# Displaying permissions
az ad app permission list --id ${ARM_CLIENT_ID}
