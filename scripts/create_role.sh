#!/bin/sh

echo "Creating Role: ${RESOURCE_GROUP}-role"
vault write ${AZ_SECRET_PATH}/roles/${RESOURCE_GROUP}-role ttl=5m azure_roles=-<<EOF
[ {
    "role_name": "Contributor",
    "scope":  "/subscriptions/${ARM_SUBSCRIPTION_ID}/resourceGroups/${RESOURCE_GROUP}"
	}]
EOF

echo "Reading Role: ${RESOURCE_GROUP}-role"
vault read ${AZ_SECRET_PATH}/roles/${RESOURCE_GROUP}-role
