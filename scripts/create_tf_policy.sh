#!/bin/sh

echo "Creating policy for Terraform"
vault policy write terraform -<<EOH
path "$AZ_SECRET_PATH/creds/$RESOURCE_GROUP-role" {
  capabilities = ["read"]
}
EOH

echo "Reading policy"
vault policy read terraform

echo "Generating a token for Terraform"
token=$(vault token create -format=json -policy=terraform -ttl=24h | jq -r .auth.client_token)
vault token lookup $token

echo "Generate Azure credentials using the command below"
echo "VAULT_TOKEN=$token vault read $AZ_SECRET_PATH/creds/$RESOURCE_GROUP-role"


