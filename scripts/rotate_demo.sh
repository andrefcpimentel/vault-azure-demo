#!/bin/sh

# Lookup Object ID
OBJECT_ID=$(az ad app list --filter "displayname eq 'Kawsar-Vault-demo'" | jq -r '.[].objectId')

[[ ! -z ${OBJECT_ID} ]] && echo "Object ID for Vault-demo is: ${OBJECT_ID}" || echo "Could not find Object ID, please check for previous errors."

# Create Role
vault write ${AZ_SECRET_PATH}/roles/rotate-demo ttl=72h application_object_id=${OBJECT_ID}

vault read -format=json ${AZ_SECRET_PATH}/creds/rotate-demo > rotate-demo.json

CLIENT_SECRET="$(cat rotate-demo.json | jq -r .password)"
vault write ${AZ_SECRET_PATH}/config client_secret=${CLIENT_SECRET}
